CONTENT_DIR := content
POSTS_DIR := $(CONTENT_DIR)/posts
PAGES_DIR := $(CONTENT_DIR)/pages
TEMPLATES_DIR := templates

# The directory to publish
BUILD_DIR := build
# Generated intermediate files
GEN_DIR := gen

config := config.yaml

# Find all markdown files
index_src := $(CONTENT_DIR)/index.md
post_srcs := $(shell find $(POSTS_DIR) -name '*.md' | sort -r)
page_srcs := $(shell find $(PAGES_DIR) -name '*.md' | sort)

# Turn them to html files
posts := $(post_srcs:$(POSTS_DIR)/%.md=$(BUILD_DIR)/%.html)
pages := $(page_srcs:$(PAGES_DIR)/%.md=$(BUILD_DIR)/%.html)
post_links := $(posts:$(BUILD_DIR)/%.html=$(GEN_DIR)/%.link.html)
page_links := $(pages:$(BUILD_DIR)/%.html=$(GEN_DIR)/%.link.html)
navbar := $(GEN_DIR)/navbar.html
index := $(BUILD_DIR)/index.html

# Templates
link_template := $(TEMPLATES_DIR)/link.html
page_template := $(TEMPLATES_DIR)/page.html
navbar_template := $(TEMPLATES_DIR)/navbar.html
navbar_link_template := $(TEMPLATES_DIR)/navbar-link.html

all: $(posts) $(pages) $(index)

clean:
	rm -rf $(BUILD_DIR) $(GEN_DIR)

$(navbar): $(navbar_template) $(page_links)
	pandoc --template=$(navbar_template) -f html $(page_links) -o $@

# Pattern rule that generates html from markdown
$(BUILD_DIR)/%.html: $(POSTS_DIR)/%.md $(page_template) $(config) $(navbar)
	pandoc --template=$(page_template) --defaults $(config) -B $(navbar) $< -s -o $@

$(BUILD_DIR)/%.html: $(PAGES_DIR)/%.md $(page_template) $(config) $(navbar)
	pandoc --template=$(page_template) --defaults $(config) -B $(navbar) $< -s -o $@

# Pattern rule that generates links to posts
# Shift headings to get a title from top-level heading
$(GEN_DIR)/%.link.html: $(POSTS_DIR)/%.md $(link_template)
	pandoc --template=$(link_template) --shift-heading-level-by=-1 -V href=$(notdir $(basename $<)).html $< -s -o $@

# Pattern rule that generates links to pages
# Shift headings to get a title from top-level heading
$(GEN_DIR)/%.link.html: $(PAGES_DIR)/%.md $(navbar_link_template)
	pandoc --template=$(navbar_link_template) --shift-heading-level-by=-1 -V href=$(notdir $(basename $<)).html $< -s -o $@

# Build index file with links to posts
$(index): $(page_template) $(post_links) $(navbar) $(config) $(index_src)
	pandoc --template=$(page_template) --defaults $(config) -B $(navbar) $(foreach link,$(post_links),-A $(link)) $(index_src) -s -o $@

$(shell mkdir -p $(BUILD_DIR) $(GEN_DIR))
